﻿namespace TestLib2

//each assembly will need to define what is the attribute that will mark functions for export
type TestLib2ExportAttrib () = inherit System.Attribute()

//methods made static so that I do not have instance this class in order to invoke

type Test () =
    [<TestLib2ExportAttrib>]
    static member Greeting i = printfn "Hello %d" i
    [<TestLib2ExportAttrib>]
    static member Echo str = sprintf "Echo, %s" str

type AnotherTest () =
    [<TestLib2ExportAttrib>]    
    static member Hello () =  printfn "Hello!"
    [<TestLib2ExportAttrib>]
    static member AddTwoNumbers x y = x + y
