﻿namespace TestLib1


//each assembly will need to define what is the attribute that will mark functions for export
type TestLib1ExportAttrib () = inherit System.Attribute()

//methods made static so that I do not have instance this class in order to invoke

type Test () =
    [<TestLib1ExportAttrib>]
    static member Greeting i = printfn "Hello %d" i
    [<TestLib1ExportAttrib>]
    static member Echo str = sprintf "Echo, %s" str

type AnotherTest () =
    [<TestLib1ExportAttrib>]    
    static member Hello () =  printfn "Hello!"
    [<TestLib1ExportAttrib>]
    static member AddTwoNumbers x y = x + y
    [<TestLib1ExportAttrib>]
    static member LongRunningMethod () = 
        Async.Sleep(5000) |> Async.RunSynchronously
        0
