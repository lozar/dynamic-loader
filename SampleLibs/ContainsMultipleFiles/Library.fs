﻿namespace ContainsMultipleFiles

//each assembly will need to define what is the attribute that will mark functions for export
type ContainsMultipleFilesExportAttrib () = inherit System.Attribute()

//methods made static so that I do not have instance this class in order to invoke

type Test () =
    [<ContainsMultipleFilesExportAttrib>]
    static member Greeting i = printfn "Hello %d" i
    [<ContainsMultipleFilesExportAttrib>]
    static member Echo str = sprintf "Echo, %s" str
