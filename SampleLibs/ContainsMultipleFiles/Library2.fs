namespace ContainsMultipleFiles 

type AnotherTest () =
    [<ContainsMultipleFilesExportAttrib>]    
    static member Hello () =  printfn "Hello!"
    [<ContainsMultipleFilesExportAttrib>]
    static member AddTwoNumbers x y = x + y