﻿using System;

namespace CSharpAssembly
{
    public class CSharpAssemblyExportAttrib: System.Attribute {}

    public class Class1
    {
        [CSharpAssemblyExportAttrib]
        public static void Say() {
            Console.WriteLine("Hi");
        }
    }
}
