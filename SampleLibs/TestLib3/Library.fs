﻿namespace TestLib3

//each assembly will need to define what is the attribute that will mark functions for export
type TestLib3ExportAttrib () = inherit System.Attribute()

type AnotherTestLib3ExportAttrib () = inherit System.Attribute()

type private PrivateDoesNotExport () = inherit System.Attribute()

type internal InternalDoesNotExport () = inherit System.Attribute()

//methods made static so that I do not have instance this class in order to invoke

type TestLib3 () =
    [<TestLib3ExportAttrib>]
    static member AssemblyName () = "TestLib3"

    [<TestLib3ExportAttrib>]
    static member ReturnsOption () = Some 3

    [<TestLib3ExportAttrib>]
    static member Throws () = failwith "RIP"; 0

    [<TestLib3ExportAttrib>]
    static member ReturnsNull () = 
        let x: System.String = null 
        x

    [<PrivateDoesNotExport>]
    static member PrivateAttribDoesNotGetLoaded () = 5

    [<InternalDoesNotExport>]
    static member InternalAttribDoesNotGetLoaded () = 5

    [<TestLib3ExportAttrib>]
    static member private PrivateMethod () = 3

    [<TestLib3ExportAttrib>]
    static member internal InternalMethod () = 3

    [<AnotherTestLib3ExportAttrib>]
    static member LoadWtihDifferentAttrib () = 3

    [<PrivateDoesNotExport>]
    [<AnotherTestLib3ExportAttrib>]
    static member LoadsWithMultipleAttribs () = 3

    [<TestLib3ExportAttrib>]
    [<AnotherTestLib3ExportAttrib>]
    static member LoadsWithMultipleAttribs2 () = 3
