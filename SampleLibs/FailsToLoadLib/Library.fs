﻿namespace FailsToLoadLib

//each assembly will need to define what is the attribute that will mark functions for export
type FailsToLoadExportAttrib () = inherit System.Attribute()

//methods made static so that I do not have instance this class in order to invoke

type Test () =
    [<FailsToLoadExportAttrib>]
    static member Greeting i = printfn "Hello %d" i

type Test2 () =
    [<FailsToLoadExportAttrib>]
    static member Greeting i = printfn "Hello %d" i
