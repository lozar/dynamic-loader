namespace Tests

open NUnit.Framework
open Mailbox
open Types.ErrorTypes
open System.IO
open FSharpPlus

type TestClass () =

   [<SetUp>]
   member this.Setup () = ()

   [<TearDown>]
   member this.TearDown () = UnloadAll ()

   member this.LoadAssembly fName = 
      Path.Join([|Directory.GetCurrentDirectory();"..";"..";"..";"..";"loader";"Assemblies"; fName|])
      |> AddAssemblyByFileName
      |> Async.RunSynchronously

   [<Test>]
   member this.FakePathGivesUnableToReadFile () =
      this.LoadAssembly "q.dll" 
      |> function
        | Error UnableToReadFile -> Assert.Pass ""
        | q -> Assert.Fail (sprintf "%A" q)

   [<Test>]
   member this.WrongExtenstionGivesInvalidFileName () =
      this.LoadAssembly "TestLib1.whoops" 
      |> function
        | Error InvalidFileName -> Assert.Pass ""
        | q -> Assert.Fail (sprintf "%A" q)

   [<Test>]
   member this.LoadedTestModuleHasExpectedMethods () =
      let testList = ["AddTwoNumbers"; "Echo"; "Greeting"; "Hello"; "LongRunningMethod"]
      
      this.LoadAssembly "TestLib1.dll"
      |> function
        | Ok x -> let test = List.forall (fun y -> List.contains y testList) x
                             && x.Length = testList.Length
                  Assert.IsTrue(test)
        | x -> Assert.Fail (sprintf "%A" x)

   [<Test>]
   member this.ReturnsFileNameAlreadyLoadedWhenLoadingSameFileAgain () =
      this.LoadAssembly "TestLib1.dll"
      |> function
        | Error q -> Assert.Fail (sprintf "%A" q)
        | _ ->  ()


      this.LoadAssembly "TestLib1.dll"
      |> function
        | Error FileNameAlreadyLoaded -> Assert.Pass ""
        | q -> Assert.Fail (sprintf "%A" q)

   [<Test>]
   member this.AbleToLoadAssemblyAgainAfterUnloading () =
      this.LoadAssembly "TestLib1.dll" 
      |> function
         | Error q -> Assert.Fail (sprintf "%A" q)
         | _ -> ()

      UnloadAssemblyByFileName "TestLib1" 
      |> Async.RunSynchronously
      |> function
         | Error q -> Assert.Fail (sprintf "%A" q)
         | _ -> ()

      this.LoadAssembly "TestLib1.dll"
      |> function
         | Error q -> Assert.Fail (sprintf "%A" q)
         | _ -> Assert.Pass ""  

   [<Test>]
   member this.InvokeReturnsCorrectType () =
      this.LoadAssembly "TestLib1.dll" |> ignore  

      let t1 = invoke<string> None "Echo" [|"test"|] |> Async.RunSynchronously    
      let t2 = invoke<int> None "AddTwoNumbers" [|3;3|] |> Async.RunSynchronously

      match t1, t2 with
      | Ok "Echo, test", Ok 6 -> 
         let foo = sprintf "t1=%A t2=%A" t1 t2
         Assert.Pass foo
      | _ -> Assert.Fail (sprintf "invoke returned error %A %A" t1 t2)

   [<Test>]
   member this.InvokeUnitAsIntReturnsError () =
      this.LoadAssembly "TestLib1.dll" |> ignore  
      
      invoke<int> None "Greeting" [|10|] 
      |> Async.RunSynchronously    
      |> function
         | Error UnitReturnedOnInvoke -> Assert.Pass ""
         | x -> Assert.Fail (sprintf "%A" x)

   [<Test>]
   member this.InvalidStringToIntReturnsError () =
      this.LoadAssembly "TestLib1.dll" |> ignore  

      invoke<int> None "Echo" [|"test"|] 
      |> Async.RunSynchronously    
      |> function
         | Error CastFailure -> Assert.Pass ""
         | x -> Assert.Fail (sprintf "%A" x)

   [<Test>]
   member this.InvokeWithStringOnIntReturnsError () =
      this.LoadAssembly "TestLib1.dll" |> ignore  
      
      invoke<string> None "AddTwoNumbers" [|3;3|] 
      |> Async.RunSynchronously    
      |> function
         | Error CastFailure -> Assert.Pass ""
         | x -> Assert.Fail (sprintf "%A" x)

   [<Test>]
   member this.LoadsMoreThanOneFile () =
      [this.LoadAssembly "TestLib1.dll" ; this.LoadAssembly "TestLib2.dll"; this.LoadAssembly "TestLib3.dll"]
      |> List.forall (fun x -> match x with | Ok _ -> true | Error e -> false)
      |> Assert.IsTrue
      
   [<Test>]
   member this.LoadTwoAssembliesWithSameClassNames () =
      [this.LoadAssembly "TestLib1.dll" ; this.LoadAssembly "TestLib2.dll"]
      |> List.forall (fun x -> match x with | Ok _ -> true | Error e -> false)
      |> Assert.IsTrue     

   [<Test>]
   member this.InvokeWithFileName () =
      this.LoadAssembly "TestLib1.dll" |> ignore  
      
      invoke<string> (Some "TestLib1") "Echo" [|"test"|] 
      |> Async.RunSynchronously    
      |> function
         | Ok _ -> Assert.Pass ""
         | Error x -> Assert.Fail (sprintf "%A" x)

   [<Test>]
   member this.InvokeWithMultipleFilesWithSameFunctionFails () =
      this.LoadAssembly "TestLib1.dll" |> ignore
      this.LoadAssembly "TestLib2.dll" |> ignore

      invoke<string> None "Echo" [|"test"|] 
      |> Async.RunSynchronously    
      |> function
         | Error MultipleMethodsFound -> Assert.Pass ""
         | x -> Assert.Fail (sprintf "%A" x)

   [<Test>]
   member this.InvokeWithFileNameWithMultipleFilesWithSameMethodNames () =
      this.LoadAssembly "TestLib1.dll" |> ignore
      this.LoadAssembly "TestLib2.dll" |> ignore
      
      invoke<string> (Some "TestLib2") "Echo" [|"test"|] 
      |> Async.RunSynchronously    
      |> function
         | Ok _ -> Assert.Pass ""
         | Error x -> Assert.Fail (sprintf "%A" x)   

   [<Test>]
   member this.InvokeWithMissingMethodReturnsError () =
      invoke<string> None "Echo" [|"test"|] 
      |> Async.RunSynchronously    
      |> function
         | Error MethodNotFound -> Assert.Pass ""
         | x -> Assert.Fail (sprintf "%A" x)

   [<Test>]
   member this.InvokeWithWrongFileNameReturnsError () =
      this.LoadAssembly "TestLib3.dll" |> ignore

      invoke<string> (Some "TestLib1") "Echo" [|"test"|] 
      |> Async.RunSynchronously    
      |> function
         | Error FileNameNotInContextMap -> Assert.Pass ""
         | x -> Assert.Fail (sprintf "%A" x)

   [<Test>]
   member this.InvokeWithMonadReturn () =
      this.LoadAssembly "TestLib3.dll" |> ignore

      invoke<int option> None "ReturnsOption" [||] 
      |> Async.RunSynchronously    
      |> function
         | Ok (Some _) -> Assert.Pass ""
         | x -> Assert.Fail (sprintf "%A" x)

   [<Test>]
   member this.InvokeCallsMethodThatThrowsReturnsError () =
      this.LoadAssembly "TestLib3.dll" |> ignore

      invoke<int> None "Throws" [||] 
      |> Async.RunSynchronously    
      |> function
         | Error InvokeError -> Assert.Pass ""
         | x -> Assert.Fail (sprintf "%A" x)

   [<Test>]
   member this.InvokeUnitWorks () =
      this.LoadAssembly "TestLib1.dll" |> ignore

      invokeUnit None "Greeting" [|10|]
      |> Async.RunSynchronously
      |> function
         | Ok () -> Assert.Pass ""
         | x -> Assert.Fail (sprintf "%A" x)

   [<Test>]
   member this.InvokeUnitOnFunctionThatReturnsFails () =
      this.LoadAssembly "TestLib1.dll" |> ignore

      invokeUnit None "Echo" [|"10"|]
      |> Async.RunSynchronously
      |> function
         | Error NonUnitReturnReturnedFromInvokeUnit -> Assert.Pass ""
         | x -> Assert.Fail (sprintf "%A" x)

   [<Test>]
   member this.InvokeOnFunctionThatReturnsNullReturnsError () =
      this.LoadAssembly "TestLib3.dll" |> ignore

      invoke<System.String> None "ReturnsNull" [||]
      |> Async.RunSynchronously
      |> function
         | Error NullReturnType -> Assert.Pass ""
         | x -> Assert.Fail (sprintf "%A" x)      

   [<Test>]
   member this.PrivateAttribDoesNotLoad () =
      this.LoadAssembly "TestLib3.dll" |> ignore

      invoke<int> None "PrivateAttribDoesNotGetLoaded" [||]
      |> Async.RunSynchronously
      |> function
         | Error MethodNotFound -> Assert.Pass ""
         | x -> Assert.Fail (sprintf "%A" x)   

   [<Test>]
   member this.InternalAttribDoesNotLoad () =
      this.LoadAssembly "TestLib3.dll" |> ignore

      invoke<int> None "InternalAttribDoesNotGetLoaded" [||]
      |> Async.RunSynchronously
      |> function
         | Error MethodNotFound -> Assert.Pass ""
         | x -> Assert.Fail (sprintf "%A" x) 

   [<Test>]
   member this.InternalMethodDoesNotLoad () =
      this.LoadAssembly "TestLib3.dll" |> ignore

      invoke<int> None "InternalMethod" [||]
      |> Async.RunSynchronously
      |> function
         | Error MethodNotFound -> Assert.Pass ""
         | x -> Assert.Fail (sprintf "%A" x)    

   [<Test>]
   member this.PrivateMethodDoesNotLoad () =
      this.LoadAssembly "TestLib3.dll" |> ignore

      invoke<int> None "PrivateMethod" [||]
      |> Async.RunSynchronously
      |> function
         | Error MethodNotFound -> Assert.Pass ""
         | x -> Assert.Fail (sprintf "%A" x)   

   [<Test>]
   member this.LoadingLibWithNothingExportedReturnsError () =
      this.LoadAssembly "NonExportingLib.dll" 
      |> function
         | Error InvalidAssembly -> Assert.Pass ""
         | x -> Assert.Fail (sprintf "%A" x)          

   [<Test>]
   member this.LoadsWithAnotherPublicAttrib () =
      this.LoadAssembly "TestLib3.dll" |> ignore

      invoke<int> None "LoadWtihDifferentAttrib" [||]
      |> Async.RunSynchronously
      |> function
         | Ok _ -> Assert.Pass ""
         | x -> Assert.Fail (sprintf "%A" x)  

   [<Test>]
   member this.LoadsWithMultipleAttribsAsLongAsOneIsPublic () =
      this.LoadAssembly "TestLib3.dll" |> ignore

      invoke<int> None "LoadsWithMultipleAttribs" [||]
      |> Async.RunSynchronously
      |> function
         | Ok _ -> Assert.Pass ""
         | x -> Assert.Fail (sprintf "%A" x)        

   [<Test>]
   member this.LoadsOnlyOnceWithMultiplePublicAttribs () =
      this.LoadAssembly "TestLib3.dll" |> ignore

      invoke<int> None "LoadsWithMultipleAttribs2" [||]
      |> Async.RunSynchronously
      |> function
         | Ok _ -> Assert.Pass ""
         | x -> Assert.Fail (sprintf "%A" x)   

   [<Test>]
   member this.SameMethodNameInDifferentClassesInSameAssemblyReturnsError () =
      this.LoadAssembly "FailsToLoadLib.dll"
      |> function
         | Error MultipleMethodsFound -> Assert.Pass ""
         | x -> Assert.Fail (sprintf "%A" x)            

   [<Test>]
   member this.CSharpAssemblyWorks () =
      this.LoadAssembly "CSharpAssembly.dll" |> ignore
      
      invokeUnit None "Say" [||]
      |> Async.RunSynchronously
      |> function
         | Ok _ -> Assert.Pass ""
         | x -> Assert.Fail (sprintf "%A" x)  

   [<Test>]
   member this.AssemblyWithMultipleFilesWorks () =
      this.LoadAssembly "ContainsMultipleFiles.dll" |> ignore
      
      [invokeUnit None "Hello" [||];invokeUnit None "Greeting" [|1|]]
      |> List.map Async.RunSynchronously
      |> function
         | [Ok _; Ok _] -> Assert.Pass ""
         | x -> Assert.Fail (sprintf "%A" x)         

   [<Test>]
   member this.LongRunningMethodStillRunsAfterUnload () =
      this.LoadAssembly "TestLib1.dll" |> ignore
      
      async {
         let! firstRun = invoke<int> None "LongRunningMethod" [||] |> Async.StartChild       
         do! Async.Sleep 100//give the child thread a chance to start
         UnloadAll ()
         let! secondRun = invoke<int> None "LongRunningMethod" [||]
         let! res = firstRun
         
         match res, secondRun with
         | Ok _, Error _ -> Assert.Pass ""
         | x -> Assert.Fail (sprintf "%A" x) 
      }
      |> Async.RunSynchronously