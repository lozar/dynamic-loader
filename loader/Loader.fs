module Loader

open System
open System.Reflection
open FSharpPlus
open Types.LoaderTypes
open Types.ErrorTypes

let private functionalInvoke argList (methodInfo: MethodInfo) =
    try
        Ok { value = methodInfo.Invoke(null, argList); responseType = methodInfo.ReturnType }
    with
        | _ -> (Error InvokeError)

let private getMethodMap (assembly: Reflection.Assembly) =
    let attribFilter attribTypes = Seq.exists (fun (p: Attribute) -> Array.contains (p.GetType()) attribTypes)

    let exportedAttributes = assembly.GetExportedTypes()
                             |> Array.where (fun q -> q.IsSubclassOf(typeof<System.Attribute>))

    let methods = assembly.GetExportedTypes()
                  |> Array.collect (fun q -> q.GetMethods())
                  |> Array.where (fun q -> attribFilter exportedAttributes (q.GetCustomAttributes()))
    
    if Array.distinctBy (fun (x: MethodInfo) -> x.Name) methods |> Array.length <> Array.length methods
    then Error MultipleMethodsFound
    else methods                  
         |> Array.fold (fun acc x -> Map.add x.Name x acc) Map.empty 
         |> function
            | x when Map.isEmpty x -> Error InvalidAssembly
            | x -> Ok x
    

let private loadAssembly context path = 
    Utils.getFileStream path >>= Utils.loadContext context

let internal addAssemblyByFileName contextMap path  : Result<ContextMap*MethodName list, Types.ErrorTypes.LoaderError> =
    monad {
        let! fileName = Utils.getAndValidateFileName contextMap path
        let context = CollectibleAssemblyLoadContext()
        let! methodMap = loadAssembly context path >>= getMethodMap 
        let methodNames =  Map.fold (fun acc x _ -> x::acc ) [] methodMap
        let newContextMap = Map.add fileName {context = context; methodMap = methodMap} contextMap
        return (newContextMap, methodNames)
    }
    
//probably not the best since idk if the assembly is actually unloaded or not
let internal unloadAssemblyByFileName (contextMap: ContextMap) fileName =
    monad {
        let! contextValue = Utils.mapTryFind contextMap fileName
        contextValue.context.Unload()
        return Map.remove fileName contextMap
    }

let internal unloadAll (contextMap: ContextMap) =
    Map.iter (fun _ v -> v.context.Unload()) contextMap

let private getMethodInfo (contextMap: ContextMap) methodName fileName  = 
    monad {
        let! contextValue = Map.tryFind fileName contextMap 
                            |> Utils.optionToResult FileNameNotInContextMap
        return! Map.tryFind methodName contextValue.methodMap
                |> Utils.optionToResult MethodNotFound
    }

let private fileNameLookup (contextMap: ContextMap) methodName =
    Map.fold (fun acc key value -> if Map.containsKey methodName value.methodMap then key::acc else acc) [] contextMap
    |> function
       | [x] -> Ok x
       | _::_ -> Error MultipleMethodsFound
       | [] -> Error MethodNotFound


let internal invoke (contextMap: ContextMap) fileNameOption methodName argList = 
    let fileName = match fileNameOption with
                   | Some f -> Ok f
                   | None -> fileNameLookup contextMap methodName

    fileName
    >>= getMethodInfo contextMap methodName 
    >>= functionalInvoke argList