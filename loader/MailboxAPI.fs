module Mailbox

open MailboxInternal

let private loaderMailBox = LoaderMailbox ()

let public AddAssemblyByFileName path = loaderMailBox.AddAssemblyByFileName path

let public UnloadAssemblyByFileName fileName = loaderMailBox.UnloadAssemblyByFileName fileName

let public UnloadAll () = loaderMailBox.UnloadAll ()

let public invoke<'a> fileNameOption methodName argArr =
    async {
        return if typeof<'a> = typeof<unit>
               then Error Types.ErrorTypes.UnitRequestedFromInvoke
               else loaderMailBox.Invoke fileNameOption methodName argArr |> Async.RunSynchronously
               |> function
                  | Error e -> Error e
                  | Ok x when x.responseType.FullName = "System.Void" -> Error Types.ErrorTypes.UnitReturnedOnInvoke
                  | Ok x when isNull x.value -> Error Types.ErrorTypes.NullReturnType
                  | Ok x -> Utils.safeCast<'a> x
                  | _ -> Error Types.ErrorTypes.CastFailure
    }

let public invokeUnit fileNameOption methodName argArr =
    async {
        return loaderMailBox.Invoke fileNameOption methodName argArr 
               |> Async.RunSynchronously
               |> function
                  | Error e -> Error e
                  | Ok x when x.responseType.FullName <> "System.Void" -> Error Types.ErrorTypes.NonUnitReturnReturnedFromInvokeUnit
                  | _ -> Ok ()
    }