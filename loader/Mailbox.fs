module MailboxInternal

open System
open FSharpPlus
open Loader
open Types.LoaderTypes
open Types.ErrorTypes

type private MailboxMessage =
    AddAssemblyByFileName of AsyncReplyChannel<Result<MethodName list, LoaderError>> * string
    | UnloadAssemblyByFileName of AsyncReplyChannel<Result<unit, LoaderError>> * FileName
    | UnloadAll of unit
    | Invoke of AsyncReplyChannel<Result<MethodResponse, LoaderError>> * FileName option*string * obj array

let private loaderMailbox () = 
    let reply (ch:AsyncReplyChannel<_>) msg = ch.Reply(msg)
    let mutable contextMap : ContextMap = Map.empty

    fun (inbox: MailboxProcessor<MailboxMessage>) ->
        let rec loop() = async {
            let! msg = inbox.Receive()
            match msg with
            | AddAssemblyByFileName (ch,path) -> match addAssemblyByFileName contextMap path with
                                                 | Ok (cm, mn) -> contextMap <- cm; Ok mn
                                                 | Error e -> Error e
                                                 |> ch.Reply
            | UnloadAssemblyByFileName (ch, fileName) -> match unloadAssemblyByFileName contextMap fileName with
                                                         | Ok cm -> contextMap <- cm; Ok ()
                                                         | Error e -> Error e  
                                                         |> ch.Reply
            | UnloadAll () -> unloadAll contextMap
                              contextMap <- Map.empty                                                       
            | Invoke (ch, fileNameOption, methodName, argArr) -> invoke contextMap fileNameOption methodName argArr
                                                                 |> ch.Reply
                                                                                                                                                         
            return! loop ()
        }
        loop ()

type internal LoaderMailbox () =
    let mailbox = MailboxProcessor<MailboxMessage>.Start(loaderMailbox ())

    member this.AddAssemblyByFileName path = mailbox.PostAndAsyncReply((fun ch -> AddAssemblyByFileName(ch, path)))
    member this.UnloadAssemblyByFileName fileName = mailbox.PostAndAsyncReply((fun ch -> UnloadAssemblyByFileName(ch, fileName)))
    member this.UnloadAll () = mailbox.Post(UnloadAll())
    member this.Invoke fileNameOption methodName argArr = mailbox.PostAndAsyncReply((fun ch -> Invoke(ch, fileNameOption, methodName, argArr))) 
    member this.InvokeUnit fileNameOption methodName argArr = mailbox.PostAndAsyncReply((fun ch -> Invoke(ch, fileNameOption, methodName, argArr)))
