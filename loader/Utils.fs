module Utils

open System.IO
open Types.ErrorTypes
open Types.LoaderTypes
open FSharpPlus

let internal getFileName (path: string) = 
    try
        if Path.GetExtension(path).ToLower().Equals(".dll") |> not
        then Error InvalidFileName 
        else 
            let f = (Path.GetFileName(path))
            Ok ((Seq.take (f.Length - 4) f) |> System.String.Concat)
    with
        | _ -> Error InvalidFilePath

let internal getFileStream path =
    try
        Ok (new FileStream(path, FileMode.Open, FileAccess.Read))
    with
        | _ -> Error UnableToReadFile

let inline internal mapContains map key = 
    if Map.containsKey key map
    then Error FileNameAlreadyLoaded
    else Ok key

let internal getAndValidateFileName map path = getFileName path >>= mapContains map

let internal loadContext (context: CollectibleAssemblyLoadContext) (fs: FileStream) = 
    try
        Ok (context.LoadFromStream(fs))
    with
        | _ -> Error InvalidAssembly

let inline internal mapTryFind map key = 
    match Map.tryFind key map with
    | Some v -> Ok v
    | None -> Error ContextNotFound
        
let inline internal safeCast<'a> (o: MethodResponse) = 
    try 
        Ok (unbox<'a> o.value)
    with
       | _ -> Error CastFailure            

let inline internal optionToResult loaderError =
    function
    | Some x -> Ok x
    | None -> Error loaderError