module Types

module public ErrorTypes =

    type public LoaderError = InvalidFileName 
                              | InvalidFilePath
                              | UnableToReadFile 
                              | FileNameAlreadyLoaded 
                              | InvalidAssembly
                              | ContextNotFound
                              | CastFailure
                              | FileNameNotInContextMap
                              | MethodNotFound
                              | InvokeError
                              | MultipleMethodsFound
                              | NullReturnType   
                              | UnitReturnedOnInvoke
                              | UnitRequestedFromInvoke
                              | NonUnitReturnReturnedFromInvokeUnit                       

module LoaderTypes =

    open System
    open System.Runtime.Loader
    open System.Reflection

    type internal FileName = string

    type internal MethodName = string

    type internal MethodMap = Map<MethodName, MethodInfo>

    type internal MethodResponse = { responseType: Type; value: obj }

    type internal CollectibleAssemblyLoadContext () =
        inherit AssemblyLoadContext(true)
        override this.Load assemblyName = null

    type internal ContextMapValue = {
        context: CollectibleAssemblyLoadContext
        methodMap: MethodMap
    }

    type internal ContextMap = Map<FileName, ContextMapValue>
